import express from 'express';
import {loginUserService, registerUserService} from "../../user/service/userService";

export const authenticationRouter = express.Router();

authenticationRouter.post('/register', (req, res) => {
  registerUserService(req, res);
})

authenticationRouter.post('/login', (req, res) => {
  loginUserService(req, res);
})

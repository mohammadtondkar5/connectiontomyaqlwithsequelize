import express from 'express';
export const userRouter = express.Router();

import {
  getUserByIdService,
  updateUserService, deletedUserService
} from "../service/userService";
import {checkAuthentication} from "../../jwt/jwt";

userRouter.use(checkAuthentication);

userRouter.get('/', (req, res) => {
  getUserByIdService(req, res);
})

userRouter.put('/', (req, res) => {
  updateUserService(req, res);
})

userRouter.get('/delete', (req, res) => {
  deletedUserService(req, res);
})
import {createUserRepository, deleteUserByIdRepository, getUserByEmail,
  getUserByIdRepository , updateUserRepository} from "../repository/userRepository";
import {getTokenByUserIdAndEmail, getUserIdAndEmailByToken} from "../../jwt/jwt"
import {getHashPassword} from "../../jwt/mdf";

export async function registerUserService(req, res) {
  const checkPassword  = CompareTwoPasswords(req.body.password1,req.body.password2)
  if (!checkPassword) {
    res.json({"message": "Passwords do not match"}).status(400);
    return;
  }
  const user = await createUserRepository(req.body);
  if (user) {
    const token = getTokenByUserIdAndEmail(user.email, user.id);
    res.json({user: user, token}).status(200);
    return;
  }
  res.json({"message": "ثبت نام انجام نشد"}).status(400)
}

export async function deletedUserService(req, res) {
  console.log("req.headers.authorization",req.headers.authorization)
  const user = getUserIdAndEmailByToken(req.headers.authorization);

  await deleteUserByIdRepository(user.id);
  res.json({"message": "User deleted!"})
}

export async function updateUserService(req, res) {
  const checkPassword  = CompareTwoPasswords(req.body.password1,req.body.password2)
  if (!checkPassword) {
    res.json({"message": "Passwords do not match"}).status(400);
    return;
  }
  const user = getUserIdAndEmailByToken(req.headers.authorization);
  req.body.id = user.id
  const updateUser = await updateUserRepository(req.body);
  if(updateUser){
    const result = await getUserByIdRepository(req.body.id);
    res.send(result);
    return

  }
}

export async function loginUserService(req, res) {
  const { email, password } = req.body;
  const user = await getUserByEmail(email);
  if(!user){
    res.json({"message":"email does not exist"})
  }
  const passHash = getHashPassword(password);
  const checkPassword = CompareTwoPasswords(user.password,passHash);
  if(!checkPassword){
    res.json({"message":"password incorrect"});
    return;
  }
  const token = getTokenByUserIdAndEmail(email,user.id)
  res.json({"token":token});
  return ;
}

export async function getUserByIdService(req, res) {
  const user = getUserIdAndEmailByToken(req.headers.authorization);
  const result = await getUserByIdRepository(user.id)
  res.json(result);
}

function CompareTwoPasswords(password1, password2) {
  if(password1 !== password2){
    return false
  }
  return true
  
}
import {getDbConnection} from "../../configs/mysql";
import {DataTypes} from "sequelize";
import {getHashPassword} from "../../jwt/mdf";
const UserRepository = getDbConnection().define('users', {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },

      firstName: {
        type: DataTypes.STRING(30),
        allowNull: false
      },

      lastName: {
        type: DataTypes.STRING(30),
        allowNull: false
      },

      email: {
        type: DataTypes.STRING(32),
        allowNull: false,
        unique: true
      },

      password: {
        type: DataTypes.STRING(50),
        allowNull: false
      },

      isDeleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },

      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    },
    {}
);

UserRepository.sync({force: false});

export async function createUserRepository(registerUserParams) {
  try {
    const user = await UserRepository.create({
      firstName: registerUserParams.firstName,
      lastName: registerUserParams.lastName,
      email: registerUserParams.email,
      password: getHashPassword(registerUserParams.password1),
      isDeleted: false,
    });
    return user;
  } catch (e) {
    console.log(e);
    return false
  }
}

export async function deleteUserByIdRepository(id) {
  return await UserRepository.update({
      isDeleted: true
      },
      {
        where:{
          id:id
        }
      })
}


export async function updateUserRepository(updatedUserParams) {
  return await UserRepository.update({
    firstName: updatedUserParams.firstName,
    lastName: updatedUserParams.lastName,
    password: getHashPassword(updatedUserParams.password1),
  },
      {
        where:{
          id:updatedUserParams.id
        }})
}

export async function getUserByIdRepository(id) {
  return await UserRepository.findOne({
    where: {
      id
    }
  })
}

export async function getUserByEmail(email) {
  try {
    const result = await UserRepository.findOne({where:{email:email}});
    return result.dataValues
  }catch (e) {
    return null
  }

}


import jwt from 'jsonwebtoken';

export function getTokenByUserIdAndEmail(email, id) {
  return jwt.sign({id: id, email: email}, process.env.TOKEN_SECRET, {
    expiresIn: 604800 // 1 week
  })
}

export function getUserIdAndEmailByToken(token) {
  if (token.startsWith("Bearer ")) {
    token = token.substring(7, token.length);
  }

  const decodeToken = jwt.decode(token, process.env.TOKEN_SECRET);
  return {
    id: decodeToken.id,
    email: decodeToken.email
  }
}

export function checkAuthentication(req, res, next) {
  console.log("req.headers.authentication" , req.headers.authorization)
  try {
    if (!req || !req.headers || !req.headers.authorization) {
      res.json({status: 'Authentication failed'}, 404);
      return;
    }
    // let token = req.headers.authentication;
    //
    // if (token.startsWith("Bearer ")) {
    //   token = token.substring(7, token.length);
    // }
    // const decodeToken = jwt.decode(token, process.env.TOKEN_SECRET);
    // if (!decodeToken || !decodeToken.id) {
    //   res.json({status: 'Authentication failed'}, 404);
    //   return;
    // }
    next();
  } catch (e) {
    res.json({status: 'Authentication failed'}, 404);
  }
}

import express from 'express';
import {userRouter} from "./user/controller/userController";
import bodyParser from "body-parser";
import {authenticationRouter} from "./authentication/controller/userController";

async function startServer() {
  const app = express();
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json())
  const hostname =process.env.HOST_NAME;
  const port = process.env.PORT;
  app.listen(port, hostname, () => {
    console.log(`------Server running at http://${hostname}:${port}/`);
  });


  app.use('/user', userRouter);
  app.use('/authentication', authenticationRouter);
}
startServer();



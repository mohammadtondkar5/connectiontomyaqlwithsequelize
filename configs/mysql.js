import { Sequelize } from 'sequelize';

class SqlConnection {
  static dbConnection = null;

  // Singleton pattern
  static getInstance(dbInfo) {
    if (!this.dbConnection) {
      this.dbConnection = new Sequelize(dbInfo.DB_NAME, dbInfo.DB_USERNAME,dbInfo.DB_PASSWORD, {
        host: dbInfo.DB_HOST,
        dialect: 'mysql',
        dialectOptions: {
          supportBigNumbers: true,
          bigNumberStrings: true,
          decimalNumbers: true,
        },
        sync: true,
      });

      try {
        this.dbConnection.authenticate();
        console.log('------Connection to MySql has been established successfully.');
      } catch (error) {
        console.error('-----Unable to connect to the MySql:', error);
      }
    }
    return this.dbConnection;
  }
}

export function getDbConnection() {
  const DB_NAME= process.env.DB_NAME;
  const DB_USERNAME = process.env.DB_USERNAME;
  const DB_PASSWORD = process.env.DB_PASSWORD;
  const DB_HOST = process.env.DB_HOST;
  return SqlConnection.getInstance({ DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWORD });
}